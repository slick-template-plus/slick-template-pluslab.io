{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ConstraintKinds #-}

module Main where

import           Control.Lens
import           Control.Monad
import           Data.Aeson                 as A
import           Data.Aeson.Lens
import           Development.Shake.Plus
import           Development.Shake.Plus.Forward
import           RIO
import           RIO.Time
import           Path.Binary()
import           Slick
import           Text.Mustache

import qualified Data.HashMap.Lazy as HML
import qualified RIO.Text                  as T

---Config-----------------------------------------------------------------------

siteMeta :: SiteMeta
siteMeta =
    SiteMeta { siteAuthor = "Me"
             , baseUrl = "https://example.com/"
             , siteTitle = "My Slick Site"
             , twitterHandle = Just "myslickhandle"
             , githubUser = Just "myslickgithubuser"
             }

sourceFolder :: Path Rel Dir
sourceFolder = $(mkRelDir "site/")

outputFolder :: Path Rel Dir
outputFolder = $(mkRelDir "docs/")

--Data models-------------------------------------------------------------------

withSiteMeta :: Value -> Value
withSiteMeta (Object obj) = Object $ HML.union obj siteMetaObj
  where
    Object siteMetaObj = toJSON siteMeta
withSiteMeta _ = error "only add site meta to objects"

data SiteMeta =
    SiteMeta { siteAuthor    :: String
             , baseUrl       :: String -- e.g. https://example.ca
             , siteTitle     :: String
             , twitterHandle :: Maybe String -- Without @
             , githubUser    :: Maybe String
             }
    deriving (Generic, Eq, Ord, Show, ToJSON)

-- | Data for the index page
data IndexInfo =
  IndexInfo
    { posts :: [Post]
    } deriving (Generic, Show, FromJSON, ToJSON)

type Tag = String

-- | Data for a blog post
data Post =
    Post { title       :: String
         , author      :: String
         , content     :: String
         , url         :: String
         , date        :: String
         , tags        :: [Tag]
         , description :: String
         , image       :: Maybe String
         }
    deriving (Generic, Eq, Ord, Show, FromJSON, ToJSON, Binary)

data AtomData =
  AtomData { title        :: String
           , domain       :: String
           , author       :: String
           , posts        :: [Post]
           , currentTime  :: String
           , atomUrl      :: String } deriving (Generic, ToJSON, Eq, Ord, Show)

type MonadSlick r m = (MonadReader r m, HasLogFunc r, MonadUnliftAction m, MonadThrow m)

compileTemplateP :: MonadAction m => Path Rel File -> m Template
compileTemplateP = liftAction . compileTemplate' . toFilePath

-- | given a list of posts this will build a table of contents
buildIndex :: MonadSlick r m => [Post] -> m ()
buildIndex posts' = do
  indexT <- compileTemplateP $ sourceFolder </> $(mkRelFile "templates/index.html")
  let indexInfo = IndexInfo {posts = posts'}
      indexHTML = substitute indexT (withSiteMeta $ toJSON indexInfo)
  writeFile' (outputFolder </> $(mkRelFile "index.html")) indexHTML

-- | Find and build all posts
buildPosts :: MonadSlick r m => m [Post]
buildPosts = do
  pPaths <- getDirectoryFiles sourceFolder ["posts//*.md"]
  forP pPaths $ buildPost sourceFolder

-- | Load a post, process metadata, write it to output, then return the post object
-- Detects changes to either post content or template
buildPost :: MonadSlick r m => Path Rel Dir -> Path Rel File -> m Post
buildPost srcDir srcPath = cacheAction ("build" :: T.Text, srcPath) $ do
  logInfo $ "Rebuilding post: " <> displayShow srcPath
  postContent <- readFileIn' srcDir srcPath
  -- load post content and metadata as JSON blob
  postData <- liftAction $ markdownToHTML $ postContent
  postUrl <- replaceExtension ".html" srcPath
  let withPostUrl = _Object . at "url" ?~ String (T.pack . toFilePath $ postUrl)
  -- Add additional metadata we've been able to compute
  let fullPostData = withSiteMeta . withPostUrl $ postData
  template <- compileTemplateP $ sourceFolder </> $(mkRelFile "templates/post.html")
  writeFile' (outputFolder </> postUrl) $ substitute template fullPostData
  liftAction $ convert fullPostData

-- | Copy all static files from the listed folders to their destination
copyStaticFiles :: MonadSlick r m => m ()
copyStaticFiles = do
    filepaths <- getDirectoryFiles sourceFolder ["images//*", "css//*", "js//*"]
    void $ forP filepaths $ \filepath ->
        copyFileChanged (sourceFolder </> filepath) (outputFolder </> filepath)

formatDate :: String -> String
formatDate humanDate = toIsoDate parsedTime
  where
    parsedTime =
      parseTimeOrError True defaultTimeLocale "%b %e, %Y" humanDate :: UTCTime

rfc3339 :: Maybe String
rfc3339 = Just "%H:%M:SZ"

toIsoDate :: UTCTime -> String
toIsoDate = formatTime defaultTimeLocale (iso8601DateFormat rfc3339)

buildFeed :: MonadSlick r m => [Post] -> m ()
buildFeed posts = do
  now <- liftIO getCurrentTime
  let atomData =
        AtomData
          { title = siteTitle siteMeta
          , domain = baseUrl siteMeta
          , author = siteAuthor siteMeta
          , posts = mkAtomPost <$> posts
          , currentTime = toIsoDate now
          , atomUrl = "/atom.xml"
          }
  atomTempl <- compileTemplateP $ sourceFolder </> $(mkRelFile "templates/atom.xml")
  writeFile' (outputFolder </> $(mkRelFile "atom.xml")) $ substitute atomTempl (toJSON atomData)
    where
      mkAtomPost :: Post -> Post
      mkAtomPost p = p { date = formatDate $ date p }

-- | Specific build rules for the Shake system
--   defines workflow to build the website
buildRules :: MonadSlick r m => m ()
buildRules = do
  allPosts <- buildPosts
  buildIndex allPosts
  buildFeed allPosts
  copyStaticFiles

main :: IO ()
main = do
  lo <- logOptionsHandle stderr True
  (lf, dlf) <- newLogFunc (setLogMinLevel LevelInfo lo)
  let shOpts = shakeOptions { shakeLintInside = ["\\"]}
  shakeArgsForward shOpts lf buildRules
  dlf
